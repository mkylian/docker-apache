FROM debian:10-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install -y apache2 libapache2-mod-fcgid libapache2-mod-auth-kerb && \
    apt clean

RUN rm /etc/apache2/sites-enabled/*

RUN a2enmod headers
RUN a2enmod rewrite
RUN a2enmod proxy
RUN a2enmod proxy_fcgi

COPY security.conf /etc/apache2/conf-enabled/security.conf
COPY entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh" ]
EXPOSE 80
