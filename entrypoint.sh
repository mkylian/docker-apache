#!/bin/sh

rm -f /var/run/apache2/apache2*.pid

. /etc/apache2/envvars
exec apache2 -DFOREGROUND
